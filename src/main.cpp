#include <mbed.h>

//#define SERIAL_DEBUG

#ifdef SERIAL_DEBUG
#define DEBUG_TX PA_9
#define DEBUG_RX PA_10
#endif

#define SCL PB_10
#define SDA PB_11

#define CAN_TX PA_12
#define CAN_RX PA_11

#define PulsePin PA_0
#define DirectionPin PC_15

#define StatusLEDPin PC_13
 
#define TIM_USR      TIM2
#define TIM_USR_IRQ  TIM2_IRQn

TIM_HandleTypeDef TimHandle;
void PulseTime(void);
extern "C"
void M_TIM_Handler(void) 
{
  if (__HAL_TIM_GET_FLAG(&TimHandle, TIM_FLAG_UPDATE) == SET) 
  {
    __HAL_TIM_CLEAR_FLAG(&TimHandle, TIM_FLAG_UPDATE);
    PulseTime();
  }
}

#define WDT_TIMEOUT 100

#define PROCESSING_PERIOD 0.01

#define DEBUG_PERIOD 10

#define CAN_TX_ID (0x101)

#define CAN_BITRATE 1000000
#define SERIAL_DEBUG_BITRATE 1000000
#define I2C_BITRATE 100000

#define MAGNET_LOW_LED_PERIOD   100
#define MAGNET_NOT_DETECTED_LED_PERIOD 20
#define SERIAL_DEBUG_PERIOD 10
 
#define AS560X_ADDRESS     (0x6C) 
#define STATUS_MAGNET_DETECTED     (0x20)
#define STATUS_MAGNET_LOW          (0x10)
#define TransactionError (0xFFFF)
#define TransactionSuccess (0)

#define MAGNET_NOT_DETECTED (0x10)
#define MAGNET_LOW (0x20)
#define I2C_ERROR (0x40)
#define CAN_ERROR (0x80)

#define STATUS_LED_ON false
#define STATUS_LED_OFF true
 
I2C i2c(SDA,SCL);

CAN can(CAN_RX,CAN_TX);
 
DigitalOut StatusLED(StatusLEDPin,STATUS_LED_OFF); 

DigitalOut Pulse(PulsePin,0);
DigitalOut Direction(DirectionPin,0);

#ifdef SERIAL_DEBUG
Serial pc(DEBUG_TX,DEBUG_RX,SERIAL_DEBUG_BITRATE);
#endif

Ticker ProcessTicker;

volatile bool StopMotor=true,RequestedDirection=false,ProcessTickEvent=false;

volatile uint16_t AngleSetpoint=0x1000,EncoderPeriod,EstimatedAngle,MultiturnAngle=0x8000,CurrentPeriod=5000,RequestedCurrentPeriod=5000;

typedef enum {ZMCO=0, ZPOS=0x81, MPOS=0x83, MANG=0x85, CONF=0x87, ABN=0x9, PUSHTHR=0xA, //Register address |0x80 if 16 bits field
              STATUS=0xB, RAW_ANGLE=0x8C, ANGLE=0x8E, AGC=0x1A, MAGNITUDE=0x9B}AS560XRegisterAddress;

volatile struct                //Buffered encoder registers
{
  uint8_t ZMCO=0;
  uint16_t ZPOS=0;
  uint16_t MPOS=0;
  uint16_t MANG=0;
  uint16_t CONF=0;
  uint8_t ABN=0;
  uint8_t PUSHTHR=0;
  uint8_t STATUS=0;
  uint16_t RAW_ANGLE=0;
  uint16_t ANGLE=0;
  uint8_t AGC=0;
  uint16_t MAGNITUDE=0;
}AS560XRegisters;

uint16_t AS560XRead(void)   //Read all meaningful registers
{
    char Data[16];

    Data[0]=0x00;
    if(i2c.write(AS560X_ADDRESS, Data, 1, true)) return TransactionError; // no stop
    else if(i2c.read(AS560X_ADDRESS, Data, 16, false)) return TransactionError;
 
    AS560XRegisters.ZMCO=Data[0];
    AS560XRegisters.ZPOS=((unsigned int)Data[1])<<8;
    AS560XRegisters.ZPOS|=((unsigned int)Data[2]);
    AS560XRegisters.MPOS=((unsigned int)Data[3])<<8;
    AS560XRegisters.MPOS|=((unsigned int)Data[4]);
    AS560XRegisters.MANG=((unsigned int)Data[5])<<8;
    AS560XRegisters.MANG|=((unsigned int)Data[6]);
    AS560XRegisters.CONF=((unsigned int)Data[7])<<8;
    AS560XRegisters.CONF|=((unsigned int)Data[8]);
    AS560XRegisters.ABN=Data[9];
    AS560XRegisters.PUSHTHR=Data[10];
    AS560XRegisters.STATUS=Data[11];
    AS560XRegisters.RAW_ANGLE=((unsigned int)Data[12])<<8;
    AS560XRegisters.RAW_ANGLE|=((unsigned int)Data[13]);
    AS560XRegisters.ANGLE=((unsigned int)Data[14])<<8;
    AS560XRegisters.ANGLE|=((unsigned int)Data[15]);
  
    Data[0]=0x1A;
    if(i2c.write(AS560X_ADDRESS, Data, 1, true)) return TransactionError; // no stop
    else if(i2c.read(AS560X_ADDRESS, Data, 3, false)) return TransactionError;
  
    AS560XRegisters.AGC=Data[0];
    AS560XRegisters.MAGNITUDE=((unsigned int)Data[1])<<8;
    AS560XRegisters.MAGNITUDE|=((unsigned int)Data[2]);
  
    return TransactionSuccess;
}

uint16_t AS560XRead(AS560XRegisterAddress Address)  //Read one register
{
  char Data[2]={(uint8_t)Address,0};

  if(Data[0]&0x80)
  {
    Data[0]&=~0x80;
    if(i2c.write(AS560X_ADDRESS, Data, 1, true)) return TransactionError; // no stop
    else if(i2c.read(AS560X_ADDRESS, Data, 2, false)) return TransactionError;

    return ((((unsigned int)Data[0])<<8)|((unsigned int)Data[1]));
  }
  else
  {
    if(i2c.write(AS560X_ADDRESS, Data, 1, true)) return TransactionError; // no stop
    else if(i2c.read(AS560X_ADDRESS, Data, 1, false)) return TransactionError;

    return ((unsigned int)Data[0]);
  }
  return TransactionError;
}

void ProcessTick(void)
{
    ProcessTickEvent=true;
}

void Process(void)
{
#ifdef SERIAL_DEBUG
    static uint16_t tConsole=0;
#endif
    static uint16_t tStatusLED=0;
    static bool I2CError=false,CANError=false;
    uint16_t EstimatedAngleLocal,ScaledEstimatedAngle,AngleEstimationError;
    uint8_t Data[6];

    if(AS560XRead())
    {
        I2CError = true;
        i2c.abort_transfer();
        i2c.write(0x00);
    }
    else I2CError=false;

    if(can.tderror())
    {
        CANError=true;
        can.reset();
        can.frequency(CAN_BITRATE); // Set bit rate to 1Mbps
    }
    else CANError=false;

    Data[0]=(uint8_t)(AS560XRegisters.RAW_ANGLE&0xFF);
    Data[1]=(uint8_t)((AS560XRegisters.RAW_ANGLE>>8)&0x0F);
    if(CANError) 
    {
        Data[1]|=CAN_ERROR;
        CANError=false;
    }
    if(I2CError) Data[1]|=I2C_ERROR;
    if((AS560XRegisters.STATUS&STATUS_MAGNET_DETECTED)==0) Data[1]|=MAGNET_NOT_DETECTED;
    if(AS560XRegisters.STATUS&STATUS_MAGNET_LOW) Data[1]|=MAGNET_LOW;
    core_util_critical_section_enter();
    EstimatedAngleLocal=EstimatedAngle;
    core_util_critical_section_exit();
    ScaledEstimatedAngle=((((EstimatedAngleLocal<<2)/5)<<2)/5)<<2;
    Data[2]=(uint8_t)(ScaledEstimatedAngle&0xFF);
    Data[3]=(uint8_t)((ScaledEstimatedAngle>>8)&0xFF);
    AngleEstimationError=abs(ScaledEstimatedAngle-MultiturnAngle);
    Data[4]=(uint8_t)(AngleEstimationError&0xFF);
    Data[5]=(uint8_t)((AngleEstimationError>>8)&0xFF);
    can.write(CANMessage(CAN_TX_ID,Data,6));        // transmit message

    if(!CANError&!I2CError)
    {
        Watchdog::get_instance().kick();
        if(AS560XRegisters.STATUS&STATUS_MAGNET_DETECTED)
        {
            if(AS560XRegisters.STATUS&STATUS_MAGNET_LOW)
            {
                if(++tStatusLED>=MAGNET_LOW_LED_PERIOD)
                {
                    tStatusLED=0;
                    StatusLED=!StatusLED;
                }
            }
            else
            {
                tStatusLED=0;
                StatusLED=STATUS_LED_ON;
            }
        }
        else
        {
            if(++tStatusLED>=MAGNET_NOT_DETECTED_LED_PERIOD)
            {
                tStatusLED=0;
                StatusLED=!StatusLED;
            }
        }
    }
    else
    {
        tStatusLED=0;
        StatusLED=STATUS_LED_OFF;
    }
    
    CANMessage RXMsg;
    if(can.read(RXMsg)) if(RXMsg.id==0x100)AngleSetpoint=((((uint16_t)RXMsg.data[0])&0xFF)|(((uint16_t)RXMsg.data[1])<<8));

#ifdef SERIAL_DEBUG
    if(++tConsole>=DEBUG_PERIOD)
    {
        tConsole=0;
        pc.printf("ZMCO:%01u ZPOS:%04u MPOS:%04u MANG:%04u CONF:0x%04X ABN:0x%01X PUSHT:%03u",AS560XRegisters.ZMCO,AS560XRegisters.ZPOS,AS560XRegisters.MPOS,AS560XRegisters.MANG,AS560XRegisters.CONF,AS560XRegisters.ABN,AS560XRegisters.PUSHTHR);
        pc.printf(" STAT:0x%02X R_ANG:%03uº ANG:%03uº AGC:%03u MAG:%04u\r",AS560XRegisters.STATUS,(uint16_t)((((uint32_t)AS560XRegisters.RAW_ANGLE)*360)/4096),(uint16_t)((((uint32_t)AS560XRegisters.ANGLE*360))/4096),AS560XRegisters.AGC,AS560XRegisters.MAGNITUDE);
    }
#endif
}

void MotorProcess(void)
{
    static uint16_t LastAngle,NewAngle;
    static uint8_t tStop=0;
    
    NewAngle=AS560XRegisters.RAW_ANGLE;     //Read encoder angle

    if(abs(NewAngle-(MultiturnAngle&0x0FFF))>2048)  //Adjust for multiturn operation
    {
        if(NewAngle>(MultiturnAngle&0x0FFF)) MultiturnAngle-=0x1000;
        else MultiturnAngle+=0x1000;
    }
    MultiturnAngle&=~0x0FFF;
    MultiturnAngle|=(NewAngle&0x0FFF);

    if(StopMotor)                           //Cyclically change the setpoint to achieve continous operation
    {
        if(++tStop>100)
        {
            if(AngleSetpoint==0x1000)AngleSetpoint=0xF000;
            else AngleSetpoint=0x1000;
        }
    }
    else tStop=0;

    if(abs(MultiturnAngle-AngleSetpoint)<5) //If we are close enough to the setpoint, stop
    {                                       //and set low frequency interrupt (10ms)
        StopMotor=true;
        EncoderPeriod=0;
        CurrentPeriod=10000;
    }
    else if(abs(MultiturnAngle-AngleSetpoint)>10)   //If we are far enough start
    {
        StopMotor=false;
        RequestedDirection=(MultiturnAngle>AngleSetpoint)?0:1;  //Request direction of movement

        EncoderPeriod-=EncoderPeriod>>2;                        //Moving average speed sensing
        if(Direction)
        {
            if(MultiturnAngle>LastAngle)EncoderPeriod+=(MultiturnAngle-LastAngle)>>2;
        }
        else if(MultiturnAngle<LastAngle)EncoderPeriod+=(LastAngle-MultiturnAngle)>>2;
        if(EncoderPeriod==0)EncoderPeriod=1;            //Prevent div by 0

        if(abs(MultiturnAngle-AngleSetpoint)>1)CurrentPeriod=(0xFFFF/abs(MultiturnAngle-AngleSetpoint))<<1;   //Set speed proportional to error
        else CurrentPeriod=0xFFFF;
        if(CurrentPeriod<0x40)CurrentPeriod=0x40;        //Maximum speed
        if((5000/EncoderPeriod)>CurrentPeriod) CurrentPeriod=5000/EncoderPeriod;    //Don't try to run much master than the mechanical speed
        core_util_critical_section_enter();
        RequestedCurrentPeriod=CurrentPeriod;
        core_util_critical_section_exit();
    } 

    LastAngle=MultiturnAngle;       //Save the current angle for the next iteration
}

void PulseTime(void)
{
    static bool MotorStopped=true;

    if(StopMotor)
    {
        Pulse=0;
        MotorStopped=true;
    }
    else
    {
        if(MotorStopped)
        {
            MotorStopped=false;
            Direction=RequestedDirection;
        }
        else if(Pulse)
        {
            Pulse=0;
            Direction=RequestedDirection;
        }
        else
        {
            Pulse=1;
            if(Direction)
            {
                if(EstimatedAngle<0xFFFF) EstimatedAngle++;
            }
            else if(EstimatedAngle) EstimatedAngle--;
        }
    }
    
    TIM_USR->ARR=(uint32_t)RequestedCurrentPeriod;
}
 
int main()
{    
    Watchdog::get_instance().start(WDT_TIMEOUT);

    can.frequency(CAN_BITRATE); // Set bit rate to 1Mbps
 
    i2c.frequency(I2C_BITRATE);  

    __HAL_RCC_TIM2_CLK_ENABLE();

    TimHandle.Instance               = TIM_USR;
    TimHandle.Init.Prescaler         = 71;  //1us per count
    TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
    TimHandle.Init.Period            = 5000; // 0.005 sec interval at 72 MHz MCU speed
    TimHandle.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
    HAL_TIM_Base_Init(&TimHandle);
    HAL_TIM_Base_Start_IT(&TimHandle);
    
    NVIC_SetVector(TIM_USR_IRQ, (uint32_t)M_TIM_Handler);
    NVIC_EnableIRQ(TIM_USR_IRQ);

    Process();
    
    MultiturnAngle|=AS560XRegisters.RAW_ANGLE&0x0FFF;
    EstimatedAngle=12800+(((6*(AS560XRegisters.RAW_ANGLE))+((AS560XRegisters.RAW_ANGLE)>>2))>>4);

    ProcessTicker.attach(&ProcessTick,PROCESSING_PERIOD);

    for(;;)
    {
        if(ProcessTickEvent)
        {
            ProcessTickEvent=false;
            Process();
            MotorProcess();
        }
    }
}